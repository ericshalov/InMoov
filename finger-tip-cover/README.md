This project was started by Gael Langevin: www.inmoov.fr
Creative Commons License

FINGER TIP COVER
INTRODUCTION
These silicone finger tips can cover the sensors and add a terrific grip to your InMoov hand. The covers can also be used if you don’t have sensors.

INSTRUCTIONS FOR MAKING
You can find the file needed to print the mould in the Files section. There are also instructions on how to go about making the actual covers.


InMoov is controlled by Arduino, and a specialized 'Nervo Board' has been developed for this. You can buy the board here:

CREDITS
This project was started by Gael Langevin and can be found at www.inmoov.fr

CREATORS
Gael Langevin

LICENSE INFO
Creative Commons License
This work is licensed under a Creative Commons Attribution-NonCommercial 3.0 Unported License.

