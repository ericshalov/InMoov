This project was started by Gael Langevin: www.inmoov.fr
Creative Commons License

SKULL & EARS
BUY THE NERVO BOARD
INTRODUCTION
This is the module for the Skull and Ears section of the Head.

INSTRUCTIONS OF MAKING
The Assembly guide for constructing the entire head along with the Neck are in the Head and Neck module. For this module simply print all parts.

The parts in the example are printed with ABS. I already expect some remarks about these "Too" human ears...
But the head of InMoov looked flat when seen from the front, and adding human ears, helps in that matter.

Update 28/02/14 I added new ears that allow setting speakers in the head. There is no specification given for to buy the speakers. Standard small USB speakers for PC should be fine. You can use the STL Speaker1 to know the dimensions required.


Here is a link to speakers that fit:
http://www.madisoundspeakerstore.com/approx-2-fullrange/peerless-830970-2-full-range-4-ohm/


InMoov is controlled by Arduino, and a specialized 'Nervo Board' has been developed for this. You can buy the board here:


CREDITS
This project was started by Gael Langevin and can be found at www.inmoov.fr

CREATORS
Gael Langevin

LICENSE INFO
Creative Commons License
This work is licensed under a Creative Commons Attribution-NonCommercial 3.0 Unported License.
