THIS IS NOT AN OFFICIAL InMoov DISTRIBUTION - THIS IS ONLY A git CLONABLE
COPY, PROVIDED FOR THE CONVENIENCE OF GitHub USERS.

CREDITS
This project was started by Gael Langevin and can be found at www.inmoov.fr

CREATORS
Gael Langevin

LICENSE INFO
Creative Commons License
This work is licensed under a Creative Commons Attribution-NonCommercial 3.0 Unported License.
