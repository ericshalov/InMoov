This project was started by Gael Langevin: www.inmoov.fr
Creative Commons License

WRIST
INTRODUCTION
I should have made this part from the beginning, but at the time I never thought I would go that far with this project.
If you have already assembled the hand with it's rods, well first I have to say: Good job! But now let's take it a little apart again. I have put many pictures in the assembly instructions.

MODULES
This part connects up the hand to the lower arm. I advice you start with those if you haven't and then put them all together. If you want to find out have a look at Lower Arm.

INSTRUCTIONS FOR MAKING
The Files tab contains all the files you need to start printing. You will also find an assembly guide there.

This part can replace "robpart1" or "leftrobpart1" depending on which hand you did. If you have or plan to make the left hand, print the parts that begin with "leftxxxx". "cableholderwrist1", "rotawrist3" and "wristgears3" are for the right and left hand. The gears inside are fine little things and require good tunning on your printer. I printed them at 0.4 thickness in ABS. They should be well finished other wise they won't fit or slip.


InMoov is controlled by Arduino, and a specialized 'Nervo Board' has been developed for this. You can buy the board here:

CREDITS
This project was started by Gael Langevin and can be found at www.inmoov.fr

CREATORS
Gael Langevin

LICENSE INFO
Creative Commons License
This work is licensed under a Creative Commons Attribution-NonCommercial 3.0 Unported License.
