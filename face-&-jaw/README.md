This project was started by Gael Langevin: www.inmoov.fr
Creative Commons License
FACE & JAW
INTRODUCTION
The files tab contains all the files needed to print the face and the jaw. There are also assembly instructions.

MODULES
This modules contains one sub-module, the Eye Mechanism.

INSTRUCTIONS OF MAKING
There are two assembly guides and they are contained within the files tab.


InMoov is controlled by Arduino, and a specialized 'Nervo Board' has been developed for this. You can buy the board here:

CREDITS
This project was started by Gael Langevin and can be found at www.inmoov.fr

CREATORS
Gael Langevin

LICENSE INFO

This work is licensed under a Creative Commons Attribution-NonCommercial 3.0 Unported License.

