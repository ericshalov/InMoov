LEFT HAND
INTRODUCTION
The left hand shares a number of files with the right hand, though obviously some parts are mirrored.

MODULES
The files that are the same as for the right hand can be found in the Files section of Hands.

INSTRUCTIONS FOR MAKING
The STL files of the parts that are the same as those of the Right Hand and the assembly instructions for the hands can be found in the Files tab of this module.

You can find the mirrored STL for the left hand in Files tab of this module.


InMoov is controlled by Arduino, and a specialized 'Nervo Board' has been developed for this. You can buy the board here:

CREDITS
This project was started by Gael Langevin and can be found at www.inmoov.fr

CREATORS
Gael Langevin

LICENSE INFO
Creative Commons License
This work is licensed under a Creative Commons Attribution-NonCommercial 3.0 Unported License.
