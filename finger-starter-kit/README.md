FINGER STARTER KIT
INTRODUCTION
This module can be used as an extended guide to the assembly of the fingers. This is a simple little device to get you introduced to the world of InMoov. It should easily get you going to comprehend how to use an Arduino, a servo, and a robot finger. It is going to be fun. If you have never tried an Arduino and a servo together, we are going to make things very simple for you to learn. Use the assembly instructions to assemble and print this baby InMoov. Once you understand how to control this toy, you should be ready, mentally, to build the complete robot… Mmmh maybe not, we will see about that. If you don't have access to a 3d printer, you can also buy the complete kit on Shapeways

READING THE DOCUMENTATION
Follow the assembly guide in the files tab and you should be sorted.

OUTPUT OF MODULE
The module is a test run for building and controlling the entire InMoov robot. It should be fun and give you the skills and confidence for building the entire robot.

INSTRUCTIONS OF MAKING
You can find the assembly instructions, the files needed to print the Finger Starter and a basic Arduino sketch to control it in the Files section.

CREDITS
This project was started by Gael Langevin and can be found at www.inmoov.fr

CREATORS
Gael Langevin

LICENSE INFO
Creative Commons License
This work is licensed under a Creative Commons Attribution-NonCommercial 3.0 Unported License.

