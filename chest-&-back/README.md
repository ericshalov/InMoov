This project was started by Gael Langevin: www.inmoov.fr
Creative Commons License

CHEST & BACK
INTRODUCTION
The chest and back are work in progress. As soon as there is more content we will update this part.

INSTRUCTIONS OF MAKING
All the files you need to start printing the Chest & Back can be found in the Files tab. Unfortunately there is no official assembly guide yet, but if you are thinking about making one. Please get in contact!


InMoov is controlled by Arduino, and a specialized 'Nervo Board' has been developed for this. You can buy the board here:

CREDITS
This project was started by Gael Langevin and can be found at www.inmoov.fr

CREATORS
Gael Langevin

LICENSE INFO
Creative Commons License
This work is licensed under a Creative Commons Attribution-NonCommercial 3.0 Unported License.
