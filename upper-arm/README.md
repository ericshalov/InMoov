This project was started by Gael Langevin: www.inmoov.fr
Creative Commons License

BICEPS AND FOREARM
INTRODUCTION
Don't be scared or intimidated, I started this a few months ago and didn't even know what was Arduino or a servo motor!

INSTRUCTIONS FOR MAKING
The lower part of the upper limbs consist of the biceps and the forearm. Putting them together requires precise marking and constant testing.

You can find all the files needed to print the bicep in the Files tab. You'll also find a two part assembly manual there full of pictures.
InMoov is controlled by Arduino, and a specialized 'Nervo Board' has been developed for this. You can buy the board here:


OTHER RESOURCES
3D view Upper Arm
3D view Shoulder

CREDITS
This project was started by Gael Langevin and can be found at www.inmoov.fr

CREATORS
Gael Langevin

LICENSE INFO
Creative Commons License
This work is licensed under a Creative Commons Attribution-NonCommercial 3.0 Unported License.
