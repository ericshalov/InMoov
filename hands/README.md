RIGHT HAND
INTRODUCTION
This is the part most people start with: The hands. This module contains all documentation for the right hand of InMoov

MODULES
This Module contains all the files and assembly instructions for the Right Hand. The Left Hand shares a number of files with the right hand, though some parts are mirrored and thus need different files. You can find all the files related to building the left hand in the Left Hand Module.

You can add sensors to the fingertips if you like. There is a file for printing finger tips that can hold those sensors.

There is a sub-module for creating a mold with a 3D printer to cast Silicone Finger Tips.

INSTRUCTIONS FOR MAKING
You can find all the STL files needed to print the hand and the assembly guide in the Files tab of this module.

Silicon Finger Tips

Image of the Silicon Finger Tips


InMoov is controlled by Arduino, and a specialized 'Nervo Board' has been developed for this. You can buy the board here:

MORE IMAGES:
Image of InMoov Hand



CREDITS
This project was started by Gael Langevin and can be found at www.inmoov.fr

CREATORS
Gael Langevin

LICENSE INFO
Creative Commons License
This work is licensed under a Creative Commons Attribution-NonCommercial 3.0 Unported License.
