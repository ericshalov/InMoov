This project was started by Gael Langevin: www.inmoov.fr
Creative Commons License

![Eye Mechanism](eye-mechanism.jpg "Eye Mechanism")

EYE MECHANISM
=============

INTRODUCTION
------------
This module covers the eye mechanism controlling the movement of the for the InMoov robot.

INSTRUCTIONS OF MAKING
----------------------
The assembly guide for creating the eye mechanism is the the Files tab, the .stl files for printing the parts are also in there too.


InMoov is controlled by Arduino, and a specialized 'Nervo Board' has been developed for this. You can buy the board here:


CREDITS
-------
This project was started by Gael Langevin and can be found at www.inmoov.fr

CREATORS
--------
Gael Langevin

LICENSE INFO
------------
Creative Commons License
This work is licensed under a Creative Commons Attribution-NonCommercial 3.0 Unported License.
