This project was started by Gael Langevin: www.inmoov.fr
Creative Commons License

STOMACH
INTRODUCTION
The Stomach allows InMoov to swing his body sideways. It also contains a disc with the InMoov logo.

INSTRUCTIONS OF MAKING
In the Files Tab you'll find all of the files necessary to print the Stomach.

On the InMoov website you will find the tutorial how to assemble the stomach


InMoov is controlled by Arduino, and a specialized 'Nervo Board' has been developed for this. You can buy the board here:

CREDITS
This project was started by Gael Langevin and can be found at www.inmoov.fr

CREATORS
Gael Langevin

LICENSE INFO
Creative Commons License
This work is licensed under a Creative Commons Attribution-NonCommercial 3.0 Unported License.
