This project was started by Gael Langevin: www.inmoov.fr
Creative Commons License

TORSO TOP
INTRODUCTION
This module covers the top part of the torso and the way it is attached to the shoulder. The torso is work in progres, and as soon as there is more content we will update this module.

INSTRUCTIONS OF MAKING
You can download all the files to print the top part of the torso and an assembly guide in the Files tab. Let's see how we’re going to proceed. In this assembly guide we are going to build the shoulder and torso simultaneously. I would have rather do it separately, but my pictures were already done this way, and I’m not going to take the robot apart to make new pictures.


InMoov is controlled by Arduino, and a specialized 'Nervo Board' has been developed for this. You can buy the board here:

CREDITS
This project was started by Gael Langevin and can be found at www.inmoov.fr

CREATORS
Gael Langevin

LICENSE INFO
Creative Commons License
This work is licensed under a Creative Commons Attribution-NonCommercial 3.0 Unported License.
