This project was started by Gael Langevin: www.inmoov.fr
Creative Commons License

HEAD AND NECK
INTRODUCTION
This module contains the neck, jaw, skull and face. All the parts needed to make the head and the neck. For more information on the Face & Jaw, Skull & Ears and Eye mechanism see their modules.

MODULES
There are three modules contained within the Head and Neck, the two major sub-modules are the Face and Jaw and the Skull and Ears.

OUTPUT OF MODULE
The output of this module is the fully constructed Head and Neck components.

INSTRUCTIONS OF MAKING
You can find all the files needed to print the head an neck in the Files section. There are also assembly instructions.


InMoov is controlled by Arduino, and a specialized 'Nervo Board' has been developed for this. You can buy the board here:


Other Resources:
Video tutorial of Head assembly
Assembly sketches
3D view Head
3D view Eyes 

CREDITS
This project was started by Gael Langevin and can be found at www.inmoov.fr

CREATORS
Gael Langevin

LICENSE INFO
Creative Commons License
This work is licensed under a Creative Commons Attribution-NonCommercial 3.0 Unported License.
